# flask_coolweb

#### 介绍
##### 1.1 项目简介

* 一款新闻展示的Web项目，主要为用户提供最新的金融资讯、数据。
* 以抓取其他网站数据和用户发布作为新闻的主要来源。
* 基于 Flask 框架，以 前后端不分离 的形式实现具体业务逻辑。

##### 1.2 项目技术要点

* 基于 Python 3.5 + Flask 框架实现。
* 数据存储使用 Redis + MySQL 实现。
* 第三方扩展：

    * 七牛云：文件存储平台
    * 云通信：短信验证码平台
    * 布署：基于ubuntu 16.04系统，使用 Gunicorn + Nginx 进行布署。

##### 1.3 项目功能模块

新闻模块
* 首页新闻列表
* 新闻详情

用户模块
* 登录注册&个人信息修改&关注其他用户
* 新闻收藏&发布

后台管理
* 用户统计&管理
* 新闻的审核
* 新闻分类的管理

##### 1.4 数据库关联关系
图片拼命加载中...

![数据库关联关系](https://github.com/EthanYan6/pic/raw/master/%E6%95%B0%E6%8D%AE%E5%BA%93%E5%85%B3%E8%81%94%E5%85%B3%E7%B3%BB.png)

##### 1.5 目录说明
图片拼命加载中...

![项目目录结构说明](https://github.com/EthanYan6/pic/raw/master/%E6%96%B0%E7%BB%8F%E8%B5%84%E8%AE%AF%E9%A1%B9%E7%9B%AE%E6%A1%86%E6%9E%B6%E7%BB%93%E6%9E%84.png)

#### 软件架构
软件架构说明

图片拼命加载中...

![部署架构](https://github.com/EthanYan6/pic/raw/master/%E9%A1%B9%E7%9B%AE%E9%83%A8%E7%BD%B2%E6%9E%B6%E6%9E%84.png)


#### 安装教程

1. 点击下载，或者clone到本地
2. 进入文件夹使用编辑器打开py文件
3. 即可使用


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)