"""
相关的配置信息：
1.数据库配置
2.redis配置
3.session配置
4.csrf配置1

"""
from datetime import datetime, timedelta
from random import randint

from flask import current_app

from info import create_app,db,models # 导入models的作用是让整个应用程序知道有models的存在
from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand
from info.models import User
# 调用方法获取app


app = create_app("develop")

# 创建manager对象，管理app
manager = Manager(app)

# 使用Migrate关联app，db
Migrate(app,db)
# 给manager添加一条操作命令
manager.add_command("db",MigrateCommand)



# 定义方法,创建管理员对象
# @manager.option给manger添加一个脚本运行的方法
# 参数一:在调用方法的时候传递的参数名称
# 参数二:是对参数1的解释
# 参数三:目的参数,用来传递给形式参数使用的
@manager.option('-u', '--username', dest='username')
@manager.option('-p', '--password', dest='password')
def create_superuser(username,password):

    # 1.创建管理员对象
    admin = User()

    # 2.设置用户属性
    admin.nick_name = username
    admin.mobile = username
    admin.password = password
    admin.is_admin = True

    # 3.保存到数据库中
    try:
        db.session.add(admin)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return "创建失败"

    return "创建成功了"

# 为了图表好看,添加测试用户
@manager.option('-t','--test',dest="test")
def add_test_user(test):
    user_list = []

    for i in range(312800,415881):
        user = User()
        user.nick_name = "小闫%s"%i
        user.mobile = "188%08d"%i
        user.password_hash = "pbkdf2:sha256:50000$aKqdryiI$c7a6e0e7f550cf8710def5eafda02fd36547d938bad71b8a40466830764aec6e"

        user.last_login = datetime.now() - timedelta(seconds=randint(0,3600*24*31))

        user_list.append(user)

    try:
        db.session.add_all(user_list)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return "添加测试用户失败"
    return "添加测试用户成功"
# 判断是否直接使用当前模块运行程序
if __name__ == '__main__':
    # 运行app程序,开启debug,会提示报错信息,程序运行的时候改代码
    manager.run()