from flask import Blueprint
from flask import redirect
from flask import request
from flask import session

admin_blue = Blueprint("admin",__name__,url_prefix="/admin")

from . import views


# 使用请求钩子,拦截用户的请求,只有访问了admin_blue,所装饰的视图函数需要拦截
# 1.拦截的是访问了非登录页面
# 2.拦截的是普通的用户
@admin_blue.before_request
def before_request():
    if not request.url.endswith("/admin/login"):
        if not session.get("is_admin"):
            return redirect("/")